function createNewUser() {
    const user = {};
  
    user.firstName = prompt("Введіть ім'я користувача:");
    user.lastName = prompt("Введіть прізвище користувача:");
    user.birthday = prompt("Введіть дату народження:");
  
    user.getAge = function() {
      const currentDate = new Date();
      const birthDate = new Date(user.birthday);
      let age = currentDate.getFullYear() - birthDate.getFullYear();
      const monthDiff = currentDate.getMonth() - birthDate.getMonth();
      if (monthDiff < 0 || (monthDiff === 0 && currentDate.getDate() < birthDate.getDate())) {
        age--;
      }
      return age;
    };
  
    user.getPassword = function() {
      const firstNameInitial = user.firstName.charAt(0).toUpperCase();
      const lastNameLower = user.lastName.toLowerCase();
      const birthYear = user.birthday.split(".")[2];
      return firstNameInitial + lastNameLower + birthYear;
    };
  
    return user;
  }
  
  const newUser = createNewUser();
  console.log(newUser);
  console.log("Вік користувача: " + newUser.getAge());
  console.log("Пароль користувача: " + newUser.getPassword());
  